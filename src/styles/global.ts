import styled, { createGlobalStyle } from "styled-components";

import "bootstrap/dist/css/bootstrap.min.css";
export const GlobalStyle = createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  html, body, #root{
    height: 100%;
  }

  body{
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    background: #170C3A;
    font-family: 'Source Sans Pro', sans-serif;
  }

  button{
    cursor: pointer
  }
`;

export const ModalTitle = styled.div`
  display: flex;
  align-items: center;
  img {
    width: 18px;
    margin-right: 6px;
  }
`;
