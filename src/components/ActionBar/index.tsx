import React, { useState } from "react";

import {
  Container,
  SearchTools,
  SearchInput,
  SelectTagOnly,
  ButtonAdd
} from "./styles";

import { Modal, ModalHeader, ModalBody } from "reactstrap";

import PlusIcon from "../../assets/icons/Icon-Plus.svg";
import SearchIcon from "../../assets/icons/Icon-Search.svg";
import { ModalTitle } from "../../styles/global";
import NewToolForm from "./NewToolForm";
import api from "../../services/api";

export default function ActionBar({ updateAction }: any) {
  const [newTool, setNewToolVisibility] = useState({
    visibility: false
  });

  const [query, setQuery] = useState({
    value: ""
  });

  const [byTags, setByTags] = useState(false);

  const saveNewTool = async ({ title, description, link, tags }: any) => {
    let tagsInRealFormat: string[] = [];
    if (tags) tagsInRealFormat = tags.split(",");

    const data = {
      title,
      description,
      link,
      tags: tagsInRealFormat
    };

    await api.post(`tools`, data);
    setNewToolVisibility({ visibility: false });
    updateAction();
  };

  const searchTools = (inputValue: string, byTags: boolean) => {
    setQuery({ value: inputValue });

    let customQuery = `?q=${inputValue}`;

    if (byTags) customQuery = `?tags_like=${inputValue}`;

    updateAction(customQuery);
  };

  const toggleByTags = (byTagsValue: boolean) => {
    setByTags(byTagsValue);
    searchTools(query.value, byTagsValue);
  };

  return (
    <>
      <Container>
        <SearchTools>
          <SearchInput>
            <input
              type="text"
              value={query.value}
              onChange={e => searchTools(e.target.value, byTags)}
            />
            <img className="img-as-icon" src={SearchIcon} />
          </SearchInput>
          <SelectTagOnly>
            <input
              id="tagOnly"
              type="checkbox"
              checked={byTags}
              onChange={() => toggleByTags(!byTags)}
            />
            <label htmlFor="tagOnly">Search in tags only</label>
          </SelectTagOnly>
        </SearchTools>
        <ButtonAdd onClick={() => setNewToolVisibility({ visibility: true })}>
          <img src={PlusIcon} />
          <span>Add</span>
        </ButtonAdd>
      </Container>

      <Modal
        isOpen={newTool.visibility}
        toggle={() => setNewToolVisibility({ visibility: !newTool.visibility })}
      >
        <ModalHeader
          toggle={() =>
            setNewToolVisibility({ visibility: !newTool.visibility })
          }
        >
          <ModalTitle>
            <img src={PlusIcon} />
            Add new tool
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <NewToolForm saveNewTool={saveNewTool} />
        </ModalBody>
      </Modal>
    </>
  );
}
