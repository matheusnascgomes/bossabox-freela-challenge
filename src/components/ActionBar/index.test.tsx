import React from "react";
import renderer from "react-test-renderer";
import ActionBar from ".";

describe("Action Bar UI Test", () => {
  it("Snapshot should match", () => {
    const tree = renderer
      .create(<ActionBar updateAction={() => {}} />)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
