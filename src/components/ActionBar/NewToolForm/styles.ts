import styled from "styled-components";

export const ButtomArea = styled.div`
  display: flex;
  justify-content: flex-end;
  button {
    background-color: #170c3a;
  }
`;

export const ErrorMessage = styled.p`
  color: #ff0000;
`;
