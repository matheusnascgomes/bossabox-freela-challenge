import React from "react";
import renderer from "react-test-renderer";
import NewToolForm from "./index";

describe("NewTool Form UI Test", () => {
  it("Snapshot should match", () => {
    const tree = renderer
      .create(<NewToolForm saveNewTool={() => {}} />)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
