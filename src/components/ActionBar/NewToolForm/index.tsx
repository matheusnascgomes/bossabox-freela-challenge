import React from "react";
import { Formik } from "formik";
import * as yup from "yup";

import { FormGroup, Label, Input, Button } from "reactstrap";

import { ButtomArea, ErrorMessage } from "./styles";

import { Tool } from "../../../types";

const NewToolForm = ({ saveNewTool }: any) => {
  const initialValues: Tool = {
    title: "",
    link: "",
    description: ""
  };

  const schema = yup.object().shape({
    title: yup.string().required("Tool name is a required field"),
    link: yup.string().required("Tool link is a required field"),
    description: yup.string().required("Tool description is a required field"),
    tags: yup.string()
  });

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values: any, _: any) => {
        saveNewTool(values);
      }}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={schema}
      render={({ handleSubmit, handleChange, handleBlur, values, errors }) => (
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <Label for="toolName">Tool Name</Label>
            <Input
              type="text"
              id="toolName"
              name="title"
              value={values.title}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Ex: hotel"
            />
            <ErrorMessage> {errors.title && errors.title} </ErrorMessage>
          </FormGroup>
          <FormGroup>
            <Label for="toolLink">Tool Link</Label>
            <Input
              type="text"
              id="toolLink"
              name="link"
              value={values.link}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Ex: https://github.com/typicode/hotel"
            />
            <ErrorMessage> {errors.link && errors.link} </ErrorMessage>
          </FormGroup>
          <FormGroup>
            <Label for="toolDescription">Tool Description</Label>
            <Input
              type="text"
              id="toolDescription"
              name="description"
              value={values.description}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Ex:This is a great description"
            />
            <ErrorMessage>
              {errors.description && errors.description}
            </ErrorMessage>
          </FormGroup>
          <FormGroup>
            <Label for="toolTags">
              Tags <small>split in commas</small>
            </Label>
            <Input
              type="text"
              id="toolTags"
              name="tags"
              value={values.tags}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="node, organization, webapps"
            />
          </FormGroup>
          <ButtomArea>
            <Button>
              <span>Add tool</span>
            </Button>
          </ButtomArea>
        </form>
      )}
    />
  );
};

export default NewToolForm;
