import styled from "styled-components";

export const Container = styled.div`
  font-size: 12px;
  margin-top: 45px;
  height: 32px;
  width: 100%;
  display: flex;
  justify-content: space-between;

  .img-as-icon {
    width: 20px;
  }
`;

export const SearchTools = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const SearchInput = styled.div`
  position: relative;
  input {
    width: 90%;
    background-color: #f5f4f6;
    padding: 4px 4px 4px 27px;
    height: 32px;
    border: none;
    border-radius: 4px;

    :hover {
      background-color: #c7c4cd;
    }
  }

  img {
    position: absolute;
    float: right;
    left: 4px;
    top: 8px;
  }

  .img-as-icon {
    width: 14px;
  }
`;
export const SelectTagOnly = styled.div`
  display: flex;
  align-items: center;
  color: #c7c4cd;
  input {
    margin-right: 4px;
    height: 16px;
    width: 16px;
  }

  label {
    margin-bottom: 0;
  }
`;

export const ButtonAdd = styled.a`
  cursor: pointer;
  width: 50px;
  background-color: #f5f4f6;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  border-radius: 4px;

  img {
    width: 16px;
    margin-right: 8px;
  }

  span {
    font-weight: bolder;
  }

  :hover {
    background-color: #c7c4cd;
  }
`;
