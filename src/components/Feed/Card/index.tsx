import React from "react";

import { Container, Header, Content, TagsWrapper } from "./styles";

import CloseIcon from "../../../assets/icons/Icon-Close-2px.svg";

export default function Card({ info, prepareToRemove }: any) {
  const { description, link, tags, title } = info;

  return (
    <>
      <Container>
        <Header>
          <a href={link} target="_blank" rel="noopener noreferrer">
            {title}
          </a>
          <a className="action-remove" onClick={() => prepareToRemove()}>
            <img src={CloseIcon} /> remove
          </a>
        </Header>
        <Content>{description}</Content>
        <TagsWrapper>
          {tags && tags.map((tag: string) => <p key={tag}>#{tag}</p>)}
        </TagsWrapper>
      </Container>
    </>
  );
}
