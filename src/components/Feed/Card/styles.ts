import styled from "styled-components";

export const Container = styled.div`
  margin-top: 32px;
  width: 100%;
  background-color: #f5f4f6;
  height: 150px;
  padding: 16px;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;

  a {
    font-size: 18px;
    cursor: pointer;
  }

  .action-remove {
    text-decoration: none;
    color: #7d7d7d;

    img {
      width: 12px;
    }
  }
`;

export const Content = styled.div`
  margin-top: 10px;
  margin-bottom: 10px;
  max-height: 48px;

  @media (max-width: 800px) {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
`;

export const TagsWrapper = styled.div`
  font-weight: bolder;
  display: flex;
  flex-wrap: wrap;
  max-height: 38px;
  p {
    margin-right: 6px;
  }

  @media (max-width: 800px) {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
`;
