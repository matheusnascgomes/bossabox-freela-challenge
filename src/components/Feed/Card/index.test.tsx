import React from "react";
import renderer from "react-test-renderer";
import Card from "./index";

import { Tool } from "../../../types";

describe("Card Component UI Test", () => {
  it("Snapshot should match", () => {
    const tools: Tool[] = [
      { id: "", title: "", link: "", description: "", tags: ["a", "b", "c"] }
    ];

    const tree = renderer
      .create(<Card key={null} info={tools} prepareToRemove={() => {}} />)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
