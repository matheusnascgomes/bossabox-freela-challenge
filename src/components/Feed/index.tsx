import React, { useState } from "react";
import Card from "./Card";
import api from "../../services/api";
import EmptyCard from "./EmptyCard";

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { ModalTitle } from "../../styles/global";

import CloseIcon from "../../assets/icons/Icon-Close-2px.svg";

import { Tool } from "../../types";

interface ModalContent {
  id?: string;
  title: string;
}

export default function Feed({ data, updateAction }: any) {
  const [modalRemove, setModalRemoveVisibility] = useState({
    visibility: false
  });

  const [modalContent, setModalContent] = useState<ModalContent>({
    id: "",
    title: ""
  });

  const removeTool = async (id?: string) => {
    await api.delete(`tools/${id}`);
    setModalRemoveVisibility({ visibility: false });
    updateAction();
  };

  return (
    <>
      <div>
        {data.map((card: Tool) => (
          <Card
            key={card.id}
            info={card}
            prepareToRemove={() => {
              setModalRemoveVisibility({ visibility: true });
              setModalContent({ id: card.id, title: card.title });
            }}
          />
        ))}
        {data.length === 0 && <EmptyCard />}
      </div>
      <Modal
        isOpen={modalRemove.visibility}
        toggle={() =>
          setModalRemoveVisibility({ visibility: !modalRemove.visibility })
        }
      >
        <ModalHeader
          toggle={() =>
            setModalRemoveVisibility({ visibility: !modalRemove.visibility })
          }
        >
          <ModalTitle>
            <img src={CloseIcon} />
            Remove tool
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <small>
            Are you sure you want to remove{" "}
            <strong>{modalContent.title}</strong> ?
          </small>
        </ModalBody>
        <ModalFooter>
          <Button
            onClick={() =>
              setModalRemoveVisibility({ visibility: !modalRemove })
            }
          >
            <span>Cancel</span>
          </Button>
          <Button color="danger" onClick={() => removeTool(modalContent.id)}>
            <span>Yes, remove</span>
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}
