import React from "react";

import { Container, Content } from "./styles";

import UnlikeIcon from "../../../assets/icons/Icon-Unlike.svg"

export default function EmptyCard() {
  return (
    <>
      <Container>
        <Content>
          <img src={UnlikeIcon} alt="Ícone de unlike para representar a infelicidade de a lista estar vazia"/>
          <p>Não há dados à serem exibidos</p>
        </Content>
      </Container>
    </>
  );
}
