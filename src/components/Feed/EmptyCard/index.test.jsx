import React from "react";
import renderer from "react-test-renderer";
import EmptyCard from ".";

describe("EmptyCard Component UI Test", () => {
  it("Snapshot should match", () => {
    const tree = renderer.create(<EmptyCard />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
