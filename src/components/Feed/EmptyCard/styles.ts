import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #f5f4f6;
  height: 150px;
  width: 100%;
  border-radius: 4px;
  margin-top: 32px;
`;

export const Content = styled.div`
  display: flex;
  align-items: center;
  img {
    width: 42px;
    margin-right: 4px;
  }

  p {
    font-weight: bolder;
    font-size: 16px;
  }
`;
