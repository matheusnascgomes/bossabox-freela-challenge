import React from "react";

import { Container, Title, Subtitle } from "./styles";

export default function Header() {
  return (
    <Container>
      <Title>VUTTR</Title>
      <Subtitle>Very usefull tools to remenber</Subtitle>
    </Container>
  );
}
