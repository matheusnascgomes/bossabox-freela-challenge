import styled from "styled-components";

export const Container = styled.header`
  margin-top: 12%;
  color: #c7c4cd;
`;

export const Title = styled.div`
  font-size: 3rem;
  margin-bottom: 0.5em;
`;

export const Subtitle = styled.div`
  font-size: 1.5rem;
`;
