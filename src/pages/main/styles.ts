import styled from "styled-components";

export const Container = styled.div`
  margin-left: 24%;
  margin-right: 24%;

  @media (max-width: 800px) {
    margin-left: 4%;
    margin-right: 4%;
  }
`;
