import React, { useState, useEffect } from "react";
import Header from "../../components/Header";
import { Container } from "./styles";
import ActionBar from "../../components/ActionBar";
import Feed from "../../components/Feed";
import api from "../../services/api";

export default function Main() {
  const [data, setData] = useState([]);

  const fetchData = async (query = "") => {
    const { data } = await api.get(`tools${query}`);
    setData(data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Container>
      <Header />
      <ActionBar updateAction={fetchData} />
      <Feed data={data} updateAction={fetchData} />
    </Container>
  );
}
