import React from "react";
import { mount } from "enzyme";
import App from "./App";
import "./setupTest.ts";

describe("App component Test", () => {
  it("App component should mount without crashing", () => {
    mount(<App />);
  });
});
