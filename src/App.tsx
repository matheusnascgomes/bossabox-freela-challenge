import React from "react";
import { GlobalStyle } from "./styles/global";

import Main from "./pages/main";
const App = () => {
  return (
    <>
      <GlobalStyle />
      <Main />
    </>
  );
};

export default App;
