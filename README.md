# Projeto Bossabox Front-End

## Introdução

Single-page Application desenvolvida em React JS.

Este projeto foi densenvolvido majoritariamente utilizando functional componentes, para além de abordar conceitos novos do framework, tornar o código o mais minimalista e performático possível, atendendo ao objetivo proposto.

## Como executar

Instalação de dependências usando npm ou yarn:

    $ npm install
    $ yarn

Executando o projeto com npm ou yarn:

    $ npm run start
    $ yarn start

## WebService

A API da aplicação encontra-se no link abaixo e deve estar rodando na porta 3001

[FAKE API](https://gitlab.com/bossabox/challenge-fake-api/tree/master)

# Testes

Foram desenvolvidos testes em UI baseando-se em snapshots com `jest` e `react-test-renderer` nos seguintes
componentes:

- `<Header />`
- `<ActionBar />`
- `<NewToolForm />`
- `<Card />`
- `<EmptyCard />`

Foram desenvolvidos testes em montagem nos seguintes componentes abaixo utilizando `jest` e `enzyme`:

- `<Main />`
- `<App />`

Para executar todos os testes basta rodar o comando abaixo com npm ou yarn

    $ npm run test
    $ yarn test
